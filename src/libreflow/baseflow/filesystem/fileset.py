from kabaret import flow

from .filesystemobject import TrackedFileSystemObject


class FileSetItem(flow.Object):
    
    extension = flow.Param("")


class FileSetMap(flow.Map):
    
    @classmethod
    def mapped_type(cls):
        return FileSetItem


class CreateFileSetWorkingCopy(flow.Action):
    pass


class PublishFileSetChanges(flow.Action):
    pass


class OpenFileSet(flow.Action):
    pass


class FileSet(TrackedFileSystemObject):

    files = flow.Child(FileSetMap)

    create_working_copy = flow.Child(CreateFileSetWorkingCopy)
    publish_changes = flow.Child(PublishFileSetChanges)
    open = flow.Child(OpenFileSet)


class CreateFileSet(flow.Action):
    pass
